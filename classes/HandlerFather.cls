public class HandlerFather {
    
    public Boolean permission;
    
    public static List<String> countryNamesAvailable = new List<String>{
        'Spain'
    };
    
    public HandlerFather(String country) {
        OneOrg__c mc = new OneOrg__c();
        try{
            mc = OneOrg__c.getInstance(UserInfo.getProfileId()); 
        }
        catch(Exception e) {
            permission = false;
        }
        if(mc != null && countryNamesAvailable.contains(country)){
            Boolean outcome = false;
            outcome = (Boolean) mc.get(country);
            if(null != outcome){
                return;
            }else{
                permission = false;
            }
        }
        permission = false;
    }
    
    
}